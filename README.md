# 概要
Vueの勉強用
# 内容
## compo_test
参考サイトを用いてvuexを用いたチュートリアル
[vue.js+Vuexチュートリアル](https://qiita.com/_P0cChi_/items/ebf8fbf035b36218a37e)

## myapp
_Vue.js入門_ という参考書の中のvuexのチュートリアル
/store/index.jsを特に開発した（vuexの理念を理解するため）
