# myapp
## 概要
これは_Vue.js入門_ という参考書の中のvuexのチュートリアルを参考に作成したもの。
/store/index.jsを特に開発した（vuexの理念を理解するため）

以下、プロジェクトをビルドするための方法を提示する。
## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
